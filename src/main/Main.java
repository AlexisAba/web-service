package main;

import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import ws.Product;
import ws.ProductWS;
import ws.ProductWSImplService;
import ws.ProductWSImplServiceLocator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {

            ProductWSImplService ProductsImplementsService = new ProductWSImplServiceLocator();
            ProductWS product = ProductsImplementsService.getproductWSImplPort();
            
            System.out.println("First product id = " + product.findProduct(2).getId() + " and name = " + product.findProduct(2).getLibelle());
            
            for(Product productall:product.findAll()) {
            	System.out.println(productall.getId()+" "+productall.getLibelle());
            }
            System.out.println("fin");
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
	

	}

}
