/**
 * ProductWSImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package ws;

public class ProductWSImplServiceLocator extends org.apache.axis.client.Service implements ws.ProductWSImplService {

    public ProductWSImplServiceLocator() {
    }


    public ProductWSImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ProductWSImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for productWSImplPort
    private java.lang.String productWSImplPort_address = "http://localhost:4797/ws/productws";

    public java.lang.String getproductWSImplPortAddress() {
        return productWSImplPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String productWSImplPortWSDDServiceName = "productWSImplPort";

    public java.lang.String getproductWSImplPortWSDDServiceName() {
        return productWSImplPortWSDDServiceName;
    }

    public void setproductWSImplPortWSDDServiceName(java.lang.String name) {
        productWSImplPortWSDDServiceName = name;
    }

    public ws.ProductWS getproductWSImplPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(productWSImplPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getproductWSImplPort(endpoint);
    }

    public ws.ProductWS getproductWSImplPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            ws.ProductWSImplPortBindingStub _stub = new ws.ProductWSImplPortBindingStub(portAddress, this);
            _stub.setPortName(getproductWSImplPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setproductWSImplPortEndpointAddress(java.lang.String address) {
        productWSImplPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (ws.ProductWS.class.isAssignableFrom(serviceEndpointInterface)) {
                ws.ProductWSImplPortBindingStub _stub = new ws.ProductWSImplPortBindingStub(new java.net.URL(productWSImplPort_address), this);
                _stub.setPortName(getproductWSImplPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("productWSImplPort".equals(inputPortName)) {
            return getproductWSImplPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/", "productWSImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/", "productWSImplPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("productWSImplPort".equals(portName)) {
            setproductWSImplPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
